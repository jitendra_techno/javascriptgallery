const app = new Vue({
    el: '#app',
    data: {
        apiResponse: [],
        search: '',
        paginate: ['gallery'],
        filterById: ''
    },
    mounted() {
        this.$http.get('https://jsonplaceholder.typicode.com/photos').then(result => {
                this.apiResponse = result.body;
                console.log(this.apiResponse);
            }, error => {
                console.error(error);
            });
    },
    computed: {
        filterList() {
            const filteredItem = this.apiResponse.filter(item => {
                return item.title.toLowerCase().includes(this.search.toLowerCase())
            });

            if (this.filterById == 'even') {
                return filteredItem.filter(item => {
                    return item.id%2 == 0;
                });
            } else if (this.filterById == 'odd') {
                return filteredItem.filter(item => {
                    return item.id%2 != 0;
                });
            } else {
                return filteredItem;
            }

            
        }
    },
    methods: {
       
    }
});